import React from "react";
import Card from "react-bootstrap/Card";
import Logo from "../../logo.png";
import "./usercard.css";

const UserCard = ({ createdAt, lastUpdate, username, displayName, about }) => {
  const timestamp = new Date(createdAt).toDateString();

  return (
    <Card id="userCard" style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={Logo}
        alt="Profile Photo"
        height="100px"
        width="100px"
        style={{margin: '1rem auto'}}
      />
      <Card.Body>
        <Card.Title>
          <h3 id="name">@{username}</h3>
          <sub style={{color: 'gray'}}>{`Member since: ${timestamp}`}</sub>
        </Card.Title>
        <hr/>
        <Card.Text>
          <h4 id="displayname">Nickame: {displayName}</h4>
          <h5>
            {about ? about : "This user is boring and has no bio yet"}
          </h5>
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default UserCard;
