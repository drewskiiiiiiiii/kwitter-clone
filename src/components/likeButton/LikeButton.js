import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

export const LikeButton = ({messageId, likes, handleLike, style}) => {
  let currentUser = useSelector((state) => state.auth.username)
  const loading = useSelector((state) => state.likes.getLikes.loading);
  const isLiked = () => {
    for( let like of likes ) {
      if( like.username === currentUser ) {
        return 'var(--primary-green)'
      }
    } return 'var(--primary-blue)'
  } 
  const [backgroundColor, setBackgroundColor] = useState(isLiked)
  const [liked, setLiked]=useState(isLiked)
  useEffect(() => {
    setLiked(isLiked)
  },[backgroundColor])

  
  return (
    <>
    <button
      onClick={(e)=>{
        e.preventDefault()
        handleLike(e)

      }}
      style={{backgroundColor}}
      disabled={loading}
    >
      {liked === 'var(--primary-blue)' ? 'Like' : 'Unlike'} {likes.length}</button>
    </>
  )
}