import React, {useState} from "react";
import { MenuContainer } from "../components";
import { Link } from "react-router-dom";
import {actions} from '../redux/actions/users';
import {useDispatch, useSelector} from 'react-redux';


export const AddUserPage = (state) => {
  const [username, setUsername ] = useState('');
  const [displayName, setDisplayName ] = useState('');
  const [password, setPassword ] = useState('');
  const dispatch = useDispatch();
  const error=useSelector((state) => state.user.addUser.error);

  const handleSubmit = (e) => {
    console.log(username, displayName, password);
    e.preventDefault();
    const newUser = {username,displayName,password}
    dispatch(actions.addUser(newUser))
    setUsername('');
    setPassword('');
    setDisplayName('');
   // alert("You're registered! Congrats!")
  }

  let handleReset = (e) => {
    e.preventDefault();
    setDisplayName('');
    setUsername('');
    setPassword('')
  
  }

  return (
  <>
    <MenuContainer />
    <div id="registerContainer">
    <h2>Add a new user</h2>
    <form id= "adduser-form" onSubmit={(e) => handleSubmit(e)}>
        <p><label htmlFor="username">Username</label>
        
        <input
          type="text"
          name="username"
          value={username}
          placeholder="Enter your username"
          autoFocus
          required
          
          onChange={(e) => setUsername(e.target.value)}
        /></p>
        <p><label htmlFor="displayName">Display Name</label>
        <input
          type="text"
          name="displayName"
          value={displayName}
          placeholder="Enter your display name"
          required
          onChange={(e) => setDisplayName(e.target.value)}
        /></p>
        <p><label htmlFor="password">Password</label>
        <input
          type="password"
          name="password"
          value={password}
          placeholder="Enter a password"
          required
          onChange={(e) => setPassword(e.target.value)}
        /></p>
        <button><Link id='addUserLoginLink' to='/'>{`< Back to Login`}</Link></button>
        <button type="submit">
          Submit
        </button>
        <button type="clearEntries" onClick={(e) => handleReset(e)}>
          Reset
        </button>
      </form>
      {error && <p style={{ color: "red" }}>{error.message}</p>}
      </div>
  </>
)};

export default AddUserPage;
