import React, {useState, useEffect} from "react";
import { MenuContainer } from "../components";
import { Link } from "react-router-dom";
import getStoredState from "redux-persist/es/getStoredState";
import {actions} from '../redux/actions/users';
import {useDispatch, useSelector} from 'react-redux';
import UserCard from "../components/usercard/usercard";
import { LoaderIcon } from "../components/loader";


export const DisplayUsersPage = () => {
    const dispatch = useDispatch()
   // const [users, setUsers] = useState([])
   const loading = useSelector((state) => state.user.getUsers.loading);
   const users = useSelector((state) => state.user.getUsers.users)
   // console.log(users)

    useEffect(() => {
  
    dispatch(actions.getUsers());
   console.log(users)
  }, []); 

return (
  <>
    <MenuContainer />
    
    <h2>Look at all these fun people who use Kwitter:</h2>
    <div id='userList'>
      {loading && <LoaderIcon />}
     { users && users.map((user) => (
          <UserCard
             createdAt={user.createdAt}
             username={user.username}
             displayName={user.displayName}
             about={user.about}
             key={user.id}
             lastUpdate={user.updatedAt}
        />
     ) )}
     
</div>

  </>
)
}

export default DisplayUsersPage;