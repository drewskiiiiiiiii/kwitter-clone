import api from "../../utils/api";
import { actions as authActions } from './auth';

export const ADD_USER = 'ADD_USER';
export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';
export const ADD_USER_FAILURE = 'ADD_USER_FAILURE';
export const GET_USERS = 'GET_USERS';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'GET_USERS_FAILURE';
export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'DELETE_USER_FAILURE';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const GET_CURRENT_USER_SUCCESS = 'GET_CURRENT_USER_SUCCESS';
export const GET_CURRENT_USER_FAILURE = 'GET_CURRENT_USER_FAILURE';
export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE';


const _deleteUser = (username) => async(dispatch, getState) => {
    try {
        dispatch({ type: DELETE_USER })
        const payload = await api.deleteUser(username);
        dispatch({ type: DELETE_USER_SUCCESS, payload })
    } catch (err) {
        dispatch({ type: DELETE_USER_FAILURE, payload: err.message })
    }
}

const deleteUser = (username) => async(dispatch, getState) => {
    return dispatch(_deleteUser(username))
        .then(() => { return dispatch(authActions.logout()) })
}

const getUsers = () => async(dispatch, getState) => {
    // console.log('actions/users getUser action')
    try {
        dispatch({ type: GET_USERS })
            // We do not care about the result of logging out
            // as long as it succeeds
        const payload = await api.getUsers();
        dispatch({ type: GET_USERS_SUCCESS, payload })
    } catch (err) {
        /**
         * Let the reducer know that we are logged out
         */
        dispatch({ type: GET_USERS_FAILURE, payload: err });
    }
};

const addUser = (credentials) => async(dispatch, getState) => {
    try {
        dispatch({ type: ADD_USER })
        const payload = await api.addUser(credentials);
        dispatch({ type: ADD_USER_SUCCESS, payload })
    } catch (err) {
        dispatch({ type: ADD_USER_FAILURE, payload: err.message })
    }
};

const getCurrentUser = (username) => async(dispatch, getState) => {
        try {
            dispatch({ type: GET_CURRENT_USER })
            const payload = await api.getCurrentUser(username);
            dispatch({ type: GET_CURRENT_USER_SUCCESS, payload })
        } catch (err) {
            dispatch({ type: GET_CURRENT_USER_FAILURE, payload: err.message })
        }
    }
    //the following code is from Senyce, had trouble merging
const updateProfile = (username, updatedInfo) => async(dispatch, getState) => {
    try {
        dispatch({ type: UPDATE_USER })
        const payload = await api.updateProfile(username, updatedInfo);
        dispatch({ type: UPDATE_USER_SUCCESS, payload })
    } catch (err) {
        dispatch({ type: UPDATE_USER_FAILURE, payload: err.message })
    }
}


export const actions = {
    getUsers,
    addUser,
    deleteUser,
    getCurrentUser,
    updateProfile
};