import { combineReducers } from "redux";
import { authReducer } from "./auth";
import { userReducer } from "./users";
import { messagesReducer } from './messages';
import { likesReducer } from './likes';

export default combineReducers({ auth: authReducer, user: userReducer, messages: messagesReducer, likes: likesReducer });